# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Resources

* [Mapview](http://www.appcoda.com/ios-programming-101-drop-a-pin-on-map-with-mapkit-api/)
* [Geofencing1](https://spin.atomicobject.com/2015/07/16/geofencing-ios-objectivec/)
* [Geofencing2](http://www.appcoda.com/how-to-get-current-location-iphone-user/)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Problems

* Problem: Cannot get the user's location

  Solution: [self.mapView setShowsUserLocation:true]; // Important: It is false by default.
