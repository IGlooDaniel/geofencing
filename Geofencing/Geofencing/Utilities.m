//
//  Utilities.m
//  Geofencing
//
//  Created by Xueqiang Ma on 6/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (void)showSimpleAlertWithTitle:(NSString *)title message:(NSString *)message viewController:(UIViewController *)viewController{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action];
    [viewController presentViewController:alert animated:YES completion:nil];
}

+ (void)zoomToUserLocationInMapView:(MKMapView *)mapView{
    
    CLLocation *location = mapView.userLocation.location;
    if (location) {
        CLLocationCoordinate2D coordinate = location.coordinate;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 5000.0f, 5000.0f);
        [mapView setRegion:region animated:YES];
    }
}

@end