//
//  main.m
//  Geofencing
//
//  Created by Xueqiang Ma on 5/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
