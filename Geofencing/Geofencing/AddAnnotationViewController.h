//
//  AddAnnotationViewController.h
//  Geofencing
//
//  Created by Xueqiang Ma on 5/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnnotationPin.h"

@import MapKit;

@protocol AddAnnotationViewControllerDelegate;


@interface AddAnnotationViewController : UITableViewController

@property (nonatomic, strong) id <AddAnnotationViewControllerDelegate> delegate;

@end


@protocol AddAnnotationViewControllerDelegate <NSObject>

- (void)addAnnotationViewController:(AddAnnotationViewController *)controller didAddCoordinate:(CLLocationCoordinate2D)coordinate radius:(CGFloat)radius identifier:(NSString *)identifier note:(NSString *)note eventType:(EventType)eventType;

@end