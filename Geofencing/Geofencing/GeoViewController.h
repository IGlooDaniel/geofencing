//
//  ViewController.h
//  Geofencing
//
//  Created by Xueqiang Ma on 5/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

static NSString *kSavedItemsKey = @"savedItems";

@interface GeoViewController : UIViewController

@end

