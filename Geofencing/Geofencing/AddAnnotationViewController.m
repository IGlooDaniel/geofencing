//
//  AddAnnotationViewController.m
//  Geofencing
//
//  Created by Xueqiang Ma on 5/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import "AddAnnotationViewController.h"
#import "Utilities.h"
#import <CoreLocation/CoreLocation.h>


@interface AddAnnotationViewController() <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *radiusTF;
@property (weak, nonatomic) IBOutlet UITextField *noteTF;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *zoomBtn;

@property (weak, nonatomic) IBOutlet UISegmentedControl *eventTypeSC;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end


@implementation AddAnnotationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:true]; // Important: It is false by default.
    
    self.navigationItem.rightBarButtonItems = @[self.addBtn, self.zoomBtn];
    self.addBtn.enabled = false;
    self.tableView.tableFooterView = [UIView new];
}

- (IBAction)textFieldEditingChanged:(UITextField *)sender{
    self.addBtn.enabled = !(self.radiusTF.text.length==0) && !(self.noteTF.text.length==0);
}

- (IBAction)onCancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onZoomToCurrentLocation:(id)sender{
    [Utilities zoomToUserLocationInMapView:self.mapView];
}

- (IBAction)onAdd:(UIBarButtonItem *)sender {
    CLLocationCoordinate2D coordinate = self.mapView.centerCoordinate;
    CGFloat radius = self.radiusTF.text.floatValue;
    NSString *identifier = [[NSUUID new] UUIDString];
    NSString *note = self.noteTF.text;
    EventType eventType = (self.eventTypeSC.selectedSegmentIndex == 0) ? OnEntry : OnExit;
    [self.delegate addAnnotationViewController:self didAddCoordinate:coordinate radius:radius identifier:identifier note:note eventType:eventType];
}


@end