//
//  ViewController.m
//  Geofencing
//
//  Created by Xueqiang Ma on 5/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import "GeoViewController.h"
#import "AddAnnotationViewController.h"
#import "Utilities.h"

@import MapKit;


@interface GeoViewController () <MKMapViewDelegate, AddAnnotationViewControllerDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *annotationPins;

// For using map
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end


@implementation GeoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.locationManager = [CLLocationManager new];
    [self.locationManager setDelegate:self];
    [self.locationManager requestAlwaysAuthorization];
    
    [self loadAllAnnotationPins];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onLocateBtn:(UIBarButtonItem *)sender {
    [Utilities zoomToUserLocationInMapView:self.mapView];
}

#pragma mark - addAnnotationViewController
// Implementation of the function (which is called in AddAnnotationViewController)

- (void)addAnnotationViewController:(AddAnnotationViewController *)controller didAddCoordinate:(CLLocationCoordinate2D)coordinate radius:(CGFloat)radius identifier:(NSString *)identifier note:(NSString *)note eventType:(EventType)eventType{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    // Check whether the radius exceeds the limitation
    CGFloat clampedRadius = (radius > self.locationManager.maximumRegionMonitoringDistance)?self.locationManager.maximumRegionMonitoringDistance : radius;
    
    AnnotationPin *annotationPin = [[AnnotationPin alloc] initWithCoordinate:coordinate radius:clampedRadius identifier:identifier note:note eventType:eventType];
    
    [self addAnnotationPin:annotationPin];
    [self startMonitoringAnnotationPin:annotationPin];

    [self saveAllAnnotationPins];
}

#pragma mark - Loading and saving functions
// Loading annotationPins from or saving them to the device memory

- (void)loadAllAnnotationPins{
    self.annotationPins = [NSMutableArray array];
    
    NSArray *savedItems = [[NSUserDefaults standardUserDefaults] arrayForKey:kSavedItemsKey];
    if (savedItems) {
        for (id savedItem in savedItems) {
            AnnotationPin *annotationPin = [NSKeyedUnarchiver unarchiveObjectWithData:savedItem];
            if ([annotationPin isKindOfClass:[AnnotationPin class]]) {
                [self addAnnotationPin:annotationPin];
            }
        }
    }
}

- (void)saveAllAnnotationPins{
    NSMutableArray *items = [NSMutableArray array];
    for (AnnotationPin *annotationPin in self.annotationPins) {
        id item = [NSKeyedArchiver archivedDataWithRootObject:annotationPin];
        [items addObject:item];
    }
    [[NSUserDefaults standardUserDefaults] setObject:items forKey:kSavedItemsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Functions that update the model/associated views with annotationPin changes

- (void)addAnnotationPin:(AnnotationPin *)annotationPin{
    [self.annotationPins addObject:annotationPin];
    [self.mapView addAnnotation:annotationPin];
    [self addRadiusOverlayForAnnotationPin:annotationPin];
    [self updateAnnotationPinsCount];
}

- (void)removeAnnotationPin:(AnnotationPin *)annotationPin{
    [self.annotationPins removeObject:annotationPin];
    
    [self.mapView removeAnnotation:annotationPin];
    [self removeRadiusOverlayForAnnotationPin:annotationPin];
    [self updateAnnotationPinsCount];
}

- (void)updateAnnotationPinsCount{
    self.title = [NSString stringWithFormat:@"Annotations (%lu)", (unsigned long)self.annotationPins.count];
    [self.navigationItem.rightBarButtonItem setEnabled:self.annotationPins.count<20];
}


#pragma mark - MKMapViewDelegate
// For Drawing annotations on the map

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    static NSString *identifier = @"myAnnotation";
    if ([annotation isKindOfClass:[AnnotationPin class]]) {
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (![annotation isKindOfClass:[MKPinAnnotationView class]] || annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            [annotationView setCanShowCallout:YES];
            
            // Delete button
            UIButton *removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            removeButton.frame = CGRectMake(.0f, .0f, 23.0f, 23.0f);
            [removeButton setImage:[UIImage imageNamed:@"DeleteAnnotation"] forState:UIControlStateNormal];
            [annotationView setLeftCalloutAccessoryView:removeButton];
        } else {
            annotationView.annotation = annotation;
        }
        return annotationView;
    }
    return nil;
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleRenderer *circleRenderer = [[MKCircleRenderer alloc] initWithOverlay:overlay];
        circleRenderer.lineWidth = 1.0f;
        circleRenderer.strokeColor = [UIColor purpleColor];
        circleRenderer.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:.4f];
        return circleRenderer;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    AnnotationPin *annotationPin = (AnnotationPin *) view.annotation;
    [self stopMonitoringAnnotationPin:annotationPin];
    [self removeAnnotationPin:annotationPin];
    [self saveAllAnnotationPins];
}

#pragma mark - Map overlay functions
// Redrawing map overlay after adding or removing annotations

- (void)addRadiusOverlayForAnnotationPin:(AnnotationPin *)annotationPin{
    if (self.mapView) [self.mapView addOverlay:[MKCircle circleWithCenterCoordinate:annotationPin.coordinate radius:annotationPin.radius]];
}

- (void)removeRadiusOverlayForAnnotationPin:(AnnotationPin *)annotationPin{
    if (self.mapView){
        NSArray *overlays = self.mapView.overlays;
        for (MKCircle *circleOverlay in overlays) {
            if ([circleOverlay isKindOfClass:[MKCircle class]]) {
                CLLocationCoordinate2D coordinate = circleOverlay.coordinate;
                if (coordinate.latitude == annotationPin.coordinate.latitude && coordinate.longitude == annotationPin.coordinate.longitude && circleOverlay.radius == annotationPin.radius) {
                    [self.mapView removeOverlay:circleOverlay];
                    break;
                }
            }
        }
    }
}

#pragma mark - CLLocationManagerDelegate
// Request for authorization

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    [self.mapView setShowsUserLocation:status==kCLAuthorizationStatusAuthorizedAlways];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error{
    NSLog(@"Monitoring failed for region with identifer: %@", region.identifier);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Location Manager failed with the following error: %@", error);
}

#pragma mark - AnnotationPins
// Monitoring a specific region

// Create a CircularRegion with annotationPin
- (CLCircularRegion *)regionWithAnnotationPin:(AnnotationPin *)annotationPin{
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:annotationPin.coordinate radius:annotationPin.radius identifier:annotationPin.identifier];
    
    // Set both to yes later.
    [region setNotifyOnEntry:annotationPin.eventType==OnEntry];
    [region setNotifyOnExit:!region.notifyOnEntry];
    
    return region;
}

- (void)startMonitoringAnnotationPin:(AnnotationPin *)annotationPin{
    if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        [Utilities showSimpleAlertWithTitle:@"Error" message:@"Geofencing is not supported on this device!" viewController:self];
        return;
    }
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
        [Utilities showSimpleAlertWithTitle:@"Warning" message:@"Your geotification is saved but will only be activated once you grant IGloo permission to access the device location." viewController:self];
    }
    
    CLCircularRegion *region = [self regionWithAnnotationPin:annotationPin];
    // Monitor the region
    [self.locationManager startMonitoringForRegion:region];
}

- (void)stopMonitoringAnnotationPin:(AnnotationPin *)annotationPin{
    for (CLCircularRegion *circularRegion in self.locationManager.monitoredRegions) {
        if ([circularRegion isKindOfClass:[CLCircularRegion class]]) {
            if ([circularRegion.identifier isEqualToString:annotationPin.identifier]) {
                [self.locationManager stopMonitoringForRegion:circularRegion];
            }
        }
    }
}

#pragma mark - Navigation

// Preparation for navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addAnnotation"]) {
        
        UINavigationController *navigationController = segue.destinationViewController;
        AddAnnotationViewController *vc = navigationController.viewControllers.firstObject;
        [vc setDelegate:self];
    }
}

@end
