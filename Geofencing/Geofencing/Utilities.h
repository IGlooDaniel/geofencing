//
//  Utilities.h
//  Geofencing
//
//  Created by Xueqiang Ma on 6/07/2016.
//  Copyright © 2016 Daniel. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;
@import MapKit;

@interface Utilities : NSObject

+ (void)showSimpleAlertWithTitle:(NSString *)title message:(NSString *)message viewController:(UIViewController *)viewController;

+ (void)zoomToUserLocationInMapView:(MKMapView *)mapView;

@end
